(function () {
	'use strict';

	angular
		.module('hr')
		.config(['$stateProvider', '$urlRouterProvider', routes]);

	function routes ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('login', {
				url: '/login',
				templateUrl: '/public/templates/login/login.tpl.html',
				controller: 'LoginCtrl',
				controllerAs: 'login'
			})
			.state('forgotPassword', {
				url: '/forgotPassword',
				templateUrl: '/public/templates/forgot-password/forgot-password.tpl.html',
				controller: 'ForgotPasswordCtrl',
				controllerAs: 'forgot'
			})
			.state('recoveryPassword', {
				url: '/recoveryPassword/:hash',
				templateUrl: '/public/templates/recovery-password/recovery-password.tpl.html',
				controller: 'RecoveryPasswordCtrl',
				controllerAs: 'recovery'
			})
			.state('registration', {
				url: '/registration/:hash',
				templateUrl: '/public/templates/registration/registration.tpl.html',
				controller: 'RegistrationCtrl',
				controllerAs: 'registration'
			})
			.state('main', {
				url: '/main',
				templateUrl: '/public/templates/main-page/main-page.tpl.html',
				controller: 'MainCtrl',
				controllerAs: 'main'
			})
			.state('main.dashboard', {
				url: '/dashboard',
				templateUrl: '/public/templates/dashboard/dashboard.tpl.html',
				controller: 'DashboardCtrl',
				controllerAs: 'dashboard'
			})
			.state('main.me', {
				url: '/me',
				templateUrl: '/public/templates/me/me.tpl.html',
				controller: 'MeCtrl',
				controllerAs: 'me'
			})
			.state('main.me.personal', {
				url: '/personal',
				templateUrl: '/public/templates/me/personal.tpl.html'
			})
			.state('main.me.job', {
				url: '/job',
				templateUrl: '/public/templates/me/job.tpl.html'
			})
			.state('main.me.timeof', {
				url: '/timeof',
				templateUrl: '/public/templates/me/timeof.tpl.html'
			})
			.state('main.me.emergency', {
				url: '/emergency',
				templateUrl: '/public/templates/me/emergency.tpl.html'
			})
			.state('main.employees', {
				url: '/employees',
				templateUrl: '/public/templates/employees/employees.tpl.html',
				controller: 'EmployeesCtrl',
				controllerAs: 'employees'
			})
			.state('main.directory', {
				url: '/directory',
				templateUrl: '/public/templates/directory/directory.tpl.html',
				controller: 'DirectoryCtrl',
				controllerAs: 'directory'
			})
			.state('main.reports', {
				url: '/reports',
				templateUrl: '/public/templates/reports/reports.tpl.html',
				controller: 'ReportsCtrl',
				controllerAs: 'reports'
			})
			.state('main.newUser', {
				url: '/newUser',
				templateUrl: '/public/templates/new-user/new-user.tpl.html',
				controller: 'NewUserCtrl',
				controllerAs: 'newUser'
			})
			.state('main.config', {
				url: '/config',
				templateUrl: '/public/templates/config/config.tpl.html',
				controller: 'ConfigCtrl',
				controllerAs: 'config'
			});

		$urlRouterProvider.otherwise('/login');
	}

}());
