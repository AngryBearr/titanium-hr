(function () {
    'use strict';

    angular
        .module('hr')
        .factory('moment', ['$window', moment]);

    function moment ($window) {
        return $window.moment;
    }
}());
