(function () {
	'use strict';

	angular
		.module('hr')
		.factory('ConfigService', [ConfigService]);

	function ConfigService () {
		var Config = {};

		/**
		 * token for API requests
		 * @type {String}
		 */
		Config.token;

		/**
		 * Current user info
		 * @type {Object}
		 */
		Config.User = {
			id: '',
			email: '',
			name: '',
			role: ''
		};

		return Config;
	}
}());
