(function () {
    'use strict';

    angular
        .module('hr')
        .factory('_', ['$window', _]);

    function _ ($window) {
        return $window._;
    }
}());
