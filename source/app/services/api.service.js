(function () {
	'use strict';

	angular
		.module('hr')
		.factory('ApiReq', ['$http', ApiReq]);

	function ApiReq ($http) {
		var api = {};

		api.get = function (url) {
			var request = $http.get(url);

			return request;
		};

		api.post = function (url, data) {
			var request = $http.post(url, data);

			return request;
		};

		// put, delete

		return api;
	}
}());
