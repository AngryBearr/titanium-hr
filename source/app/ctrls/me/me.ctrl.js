(function () {
	'use strict';

	angular
		.module('hr')

		.controller('MeCtrl', ['$http', 'ApiReq', 'ngToast', 'ConfigService', '_', MeCtrl]);

	function MeCtrl ($http, ApiReq, ngToast, ConfigService, _) {
		var self = this;

		self.allUsersInfo = {};
		self.allDataUser = {};

		/**
		 * get all user info
		 * @param {callback}
		 * @return {void}
		 */
		function getUsersInfo (cbSuccess) {
			$http.get('/basic-info')
				.then(function (res) {
					cbSuccess(res);
				}, function error (err) {
					console.log(err.data);
				});
		}

		function findUserById (allUsers, id) {
			var user;

			user = _.findWhere(allUsers, {id_user: id});

			user = angular.copy(user);

			if (!user.languages || !user.languages.length) {
				user.languages = [];
			} else {
				user.languages = user.languages.split(',');
			}

			return user;
		}

		function init () {
			getUsersInfo(function (res) {
				self.allUsersInfo = res.data;
				self.allDataUser = findUserById(self.allUsersInfo, ConfigService.User.id);
			});
		}
		init();

		self.addNewLanguage = function addNewLanguage () {
			self.allDataUser.languages.push('');
		};

		self.deleteOneLanguage = function deleteOneLanguage (index) {
			self.allDataUser.languages.splice(index, 1);
		};

		self.updateAllData = function updateAllData () {

			self.updateData = angular.copy(self.allDataUser);

			self.updateData.languages = self.updateData.languages.join(',');
			delete self.updateData.id;

			console.log(self.updateData);
			$http({
				url: '/basic-info',
				method: 'PUT',
				data: self.updateData,
				headers: {'Token': ConfigService.token}
			}).then(function success (res) {
				ngToast.create({
					className: 'success',
					content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i>' + res.data
				});
			}, function error (err) {
				ngToast.create({
					className: 'error',
					content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i>' + err.data
				});
			});
		};

		// self.user = self.allDataUser.user_id;

		self.nextUser = function nextUser () {
			self.user_id++;
			$http.get('basic-info/'+ self.user_id)
				.then(function succes (res) {
					self.allDataUser = res.data[0];
					self.user_id = res.data[0].id_user;
					self.allDataUser.languages = self.allDataUser.languages.split(',');
					self.lang = self.allDataUser.languages;
				}, function error (err) {
					console.log(err);
				});
		};
	}

}());
