(function () {
	'use strict';
	angular
		.module('hr')
		.controller('RecoveryPasswordCtrl', ['$http', '$state', '$stateParams', 'ngToast','$timeout','ValidationService', RecoveryPasswordCtrl]);

	function RecoveryPasswordCtrl ($http, $state, $stateParams, ngToast, $timeout, ValidationService) {

		var self = this;

		self.hash = $stateParams.hash;

		self.data = {};

		var validator = new ValidationService({ controllerAs: self, hideErrorUnderInputs: true });

		if (self.hash) {
			checkHash();
		} else {
			$state.go('login');
		}

		var checkingRules = {
			hasUpperCaseLetters: /^(?=.*[A-Z])/,
			hasLowerCaseLetters: /^(?=.*[a-z])/,
			hasDigits: /^(?=.*[0-9])/,
			isEightSymbolsLongOrMore: /^(?=.{8,})/
		};

		self.checkPasswordSecurity = function checkPasswordSecurity (string) {
			self.hasUpperCaseLetters = checkingRules.hasUpperCaseLetters.test(string);
			self.hasLowerCaseLetters = checkingRules.hasLowerCaseLetters.test(string);
			self.hasDigits = checkingRules.hasDigits.test(string);
			self.isEightSymbolsLongOrMore = checkingRules.isEightSymbolsLongOrMore.test(string);
			self.passwordIsSecure = checkIfSecure();
		};

		function checkSecure () {

			var arr = [
				self.hasUpperCaseLetters,
				self.hasLowerCaseLetters,
				self.hasDigits,
				self.isEightSymbolsLongOrMore
			];

			var res = true;

			for (var i = 0; i < arr.length; i++) {
				if (arr[i] === false) {
					res = false;
				}
			}

			return res;
		}

		function checkIfSecure () {

			if (checkSecure()) {
				return (true);
			} else {
				self.passwordIsSecure = false;
			}
		}

		self.checkIfMatch = function checkIfMatch () {

			if (self.data.password1 === self.data.password2) {
				self.passwordsMatch = true;
			} else {
				self.passwordsMatch = false;
			}
		};

		function checkHash () {

			$http.get('/recoveryPassword/' + self.hash)
				.then(function success (res) {
					console.log(res.data);
				}, function error (err) {
					ngToast.create({
						className: 'error',
						content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i>' + err.data
					});
				});
		}

		self.checkPassword = function checkPassword () {

			if (self.data.password1 !== self.data.password2) {
				ngToast.create({
					className: 'error',
					content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i> Password does not match'
				});
			} else {
				self.resetPassword();
			}
		};

		self.resetPassword = function resetPassword () {

			var body = {
				password: self.data.password1
			};

			if (validator.checkFormValidity(self.recoveryPasswordForm) && self.passwordIsSecure) {
				$http.put('/recoveryPassword/' + self.hash, body)
					.then(function success () {
						ngToast.create({
							className: 'success',
							content: '<i class="fa fa-check icon" aria-hidden="true"></i> Password successfully changed'
						});
						$timeout(function goToLogin () {
							$state.go('login');
						}, 1500);
					}, function error (err) {
						ngToast.create({
							className: 'error',
							content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i>' + err.data
						});
					});
			} else {
				ngToast.create({
					className: 'error',
					content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i> Not all conditions are met'
				});
			}
		};
	}
}());
