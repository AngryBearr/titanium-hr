(function () {
	'use strict';

	angular
		.module('hr')
		.controller('ForgotPasswordCtrl', ['$http', '$state', 'ngToast', 'ValidationService', ForgotPasswordCtrl]);

	function ForgotPasswordCtrl ($http, $state, ngToast, ValidationService) {
		var self = this;

		self.messages;

		self.data = {};

		var validator = new ValidationService({ controllerAs: self, hideErrorUnderInputs: true });

		self.reset = function () {

			if (!validator.checkFormValidity(self.emailForm)) {
				var msg = '<i class="fa fa-exclamation icon" aria-hidden="true"></i>';
				msg += self.emailForm.$validationSummary[0].friendlyName;
				msg += ': ';
				msg += self.emailForm.$validationSummary[0].message;
				ngToast.create({
					className: 'error',
					content: msg
				});
				return;
			}

			$http.post('/forgotPassword', self.data)
				.then(function success () {
					ngToast.create({
						className: 'success',
						content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i> Link to recovery password send to your E-mail'
					});
					$state.go('login');
				}, function error (err) {
					ngToast.create({
						className: 'error',
						content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i>' + err.data
					});
				});
		};
	}

}());
