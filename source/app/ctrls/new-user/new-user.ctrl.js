(function () {
	'use strict';

	angular
		.module('hr')
		.controller('NewUserCtrl', ['ApiReq', 'ngToast', NewUserCtrl]);

	function NewUserCtrl (ApiReq, ngToast) {
		var self = this;

		self.body = {};

		self.addNewUser = function addNewUser () {
			ApiReq.post('/employees', self.body)
			.then(function success (res) {
				ngToast.create({
					className: 'success',
					content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i>' + res.data
				});
			}, function error (err) {
				ngToast.create({
					className: 'error',
					content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i>' + err.data
				});
			});
		};

	}

}());
