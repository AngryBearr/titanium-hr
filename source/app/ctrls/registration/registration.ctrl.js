(function () {
	'use strict';
	angular
		.module('hr')
		.controller('RegistrationCtrl', ['$http', '$state', '$stateParams', 'ngToast','$timeout','ValidationService', RegistrationCtrl]);

	function RegistrationCtrl ($http, $state, $stateParams, ngToast, $timeout, ValidationService) {

		var self = this;

		self.hash = $stateParams.hash;

		self.data = {};

		var validator = new ValidationService({ controllerAs: self, hideErrorUnderInputs: true });

		console.log(self.hash);

		if (self.hash) {
			checkHash();
		} else {
			$state.go('login');
		}

		var checkingRules = {
			hasUpperCaseLetters: /^(?=.*[A-Z])/,
			hasLowerCaseLetters: /^(?=.*[a-z])/,
			hasDigits: /^(?=.*[0-9])/,
			isEightSymbolsLongOrMore: /^(?=.{8,})/
		};

		self.checkPasswordSecurity = function checkPasswordSecurity (string) {
			self.hasUpperCaseLetters = checkingRules.hasUpperCaseLetters.test(string);
			self.hasLowerCaseLetters = checkingRules.hasLowerCaseLetters.test(string);
			self.hasDigits = checkingRules.hasDigits.test(string);
			self.isEightSymbolsLongOrMore = checkingRules.isEightSymbolsLongOrMore.test(string);
			self.passwordIsSecure = checkIfSecure();
		};

		function checkSecure () {

			var arr = [
				self.hasUpperCaseLetters,
				self.hasLowerCaseLetters,
				self.hasDigits,
				self.isEightSymbolsLongOrMore
			];

			var res = true;

			for (var i = 0; i < arr.length; i++) {
				if (arr[i] === false) {
					res = false;
				}
			}

			return res;
		}

		function checkIfSecure () {

			if (checkSecure()) {
				return (true);
			} else {
				self.passwordIsSecure = false;
			}
		}

		self.checkIfMatch = function checkIfMatch () {

			if (self.data.password1 === self.data.password2) {
				self.passwordsMatch = true;
			} else {
				self.passwordsMatch = false;
			}
		};

		function checkHash () {

			$http.get('/registration/' + self.hash)
				.then(function success (res) {
					console.log(res.data);
				}, function error (err) {
					ngToast.create({
						className: 'error',
						content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i>' + err.data
					});
				});
		}

		self.checkPassword = function checkPassword () {

			if (self.data.password1 !== self.data.password2) {
				ngToast.create({
					className: 'error',
					content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i> Password does not match'
				});
			} else {
				registration();
			}
		};

		function registration () {
			var body = {
				password: self.data.password1,
				firstName: self.data.firstName,
				lastName: self.data.lastName
			};

			if (validator.checkFormValidity(self.registrationForm) && self.passwordIsSecure) {
				$http.post('/registration/' + self.hash, body)
						.then(function success (res) {
							ngToast.create({
								className: 'success',
								content: '<i class="fa fa-check icon" aria-hidden="true"></i>' + res.data
							});
							$timeout(function goToLogin () {
								$state.go('login');
							}, 1500);
						}, function error (err) {
							ngToast.create({
								className: 'error',
								content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i>' + err.data
							});
						});
			}

		}

	}
}());
