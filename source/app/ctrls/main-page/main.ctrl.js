(function () {
	'use strict';

	angular
		.module('hr')
		.controller('MainCtrl', ['ConfigService', '$state', 'ngToast', MainCtrl]);

	function MainCtrl (ConfigService, $state, ngToast) {
		var self = this;

		self.consoleCurrentUser = function consoleCurrentUser () {
			console.log(ConfigService.token);
			console.log(ConfigService.User);
		};

		if (ConfigService.token === undefined) {
			$state.go('login');
			ngToast.create({
				className: 'error',
				content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i> Please log in to your account!'
			});
		}

		self.consoleCurrentUser();
	}

}());
