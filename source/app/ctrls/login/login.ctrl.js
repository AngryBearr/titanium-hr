(function () {
	'use strict';

	angular
		.module('hr')
		.controller('LoginCtrl', ['$http', '$state', 'ngToast', 'ConfigService', 'ApiReq', 'ValidationService', '_', LoginCtrl]);

	function LoginCtrl ($http, $state, ngToast, ConfigService, ApiReq, ValidationService, _) {
		var self = this;

		self.data = {};

		var validator = new ValidationService({ controllerAs: self, hideErrorUnderInputs: true });

		self.auth = function () {
			if (validator.checkFormValidity(self.loginForm)) {
				ApiReq.post('/login', self.data)
					.then(function success (res) {
						ConfigService.token = res.data.token;
						ConfigService.User.id = res.data.data[0].id;
						ngToast.create({
							className: 'success',
							content: '<i class="fa fa-check icon" aria-hidden="true"></i> Login successfull'
						});
						$state.go('main.me.personal');
					}, function error (err) {
						ngToast.create({
							className: 'error',
							content: '<i class="fa fa-exclamation icon" aria-hidden="true"></i>' + err.data
						});
					});
			} else {
				_.each(self.loginForm.$validationSummary, function (e) {
					var str = '<i class="fa fa-exclamation icon" aria-hidden="true"></i>';
					str += ' ' + e.friendlyName + ': ';
					str += e.message;

					ngToast.create({
						className: 'error',
						content: str
					});
				});
			}

		};

	}

}());
