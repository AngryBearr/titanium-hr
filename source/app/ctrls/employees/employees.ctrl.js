(function () {
	'use strict';

	angular
		.module('hr')
		.controller('EmployeesCtrl', ['$http', 'ApiReq', EmployeesCtrl]);

	function EmployeesCtrl ($http, ApiReq) {
		var self = this;

		ApiReq.get('/employees')
			.then(function success (res) {
				self.users = res.data;
			}, function error (err){
				console.log(err);
			});
	}

}());
