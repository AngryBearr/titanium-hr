(function () {
	'use strict';

	angular
		.module('hr')
		.controller('DirectoryCtrl', ['$http', 'ApiReq', 'moment', '_', '$location', '$anchorScroll', DirectoryCtrl]);

	function DirectoryCtrl ($http, ApiReq, moment, _, $location, $anchorScroll) {
		var self = this;

		self.users = [];
		self.groupedByLetter = {};
		self.alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G' ,'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S' , 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

		self.goToLetter = function goToLetter (letter) {
			console.log(letter);
			$location.hash(letter);

			$anchorScroll();
		};

		ApiReq.get('/employees')
			.then(function success (res) {
				self.users = res.data;
				for (var i = 0; i < self.users.length; i++) {
					self.users[i].hire_date = moment.unix(res.data[i].hire_date).format('MM/DD/YYYY');
				}

				self.groupedByLetter = _.groupBy(self.users, function (user) {
					return user.first_name.substr(0, 1);
				});

			}, function error (err){
				console.log(err);
			});

	}

}());
