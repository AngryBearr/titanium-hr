angular.module('hr', [
	'ui.router',
	'ngToast',
	'ghiscoding.validation',
	'pascalprecht.translate',
	'angular-loading-bar',
	'ngAnimate'
]);

angular.module('hr').config(['$translateProvider', function ($translateProvider) {
	$translateProvider.useStaticFilesLoader({
		prefix: '/public/assets/validation/',
		suffix: '.json'
	});

	// define translation maps you want to use on startup
	$translateProvider.preferredLanguage('en');
	$translateProvider.useSanitizeValueStrategy('sanitize');
}]);

angular.module('hr').config(['ngToastProvider', function (ngToastProvider) {
	ngToastProvider.configure({
		dismissButton: true,
		dismissOnClick: false,
		dismissButtonHtml: '<i class="fa fa-times" aria-hidden="true"></i>'
	});
}]);
