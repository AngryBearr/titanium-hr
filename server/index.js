var express = require('express');
var app = express();

app.use('/public', express.static(__dirname + './../public'));

var HRAuth = require('./app/middlewares/auth.js');
app.use(HRAuth.checkAuth);

var bodyParser = require('body-parser');
app.use(bodyParser.json());

require('./routes').routes(app);

var server = app.listen(8001, function () {
	console.log('--- Start server on port ' + server.address().port + ' ---');
});
