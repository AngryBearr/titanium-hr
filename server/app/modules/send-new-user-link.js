var nodemailer = require('nodemailer');

var mail = require('./mail-config.js').configMail;
var configHost = require('./mail-config.js').host;

module.exports.sendLinkToMail = function sendLinkToMail (token, email) {

	var transporter = nodemailer.createTransport(mail.configMail);
	var toLink = configHost.host + '#/registration/' + token;
	var mailOptions = {
		from: 'nodemailertesttest@gmail.com',
		to: email, //
		subject: 'Registration new user Titanium-soft ',
		text: toLink,
		html: '<b>Hello!</b> <a href="' + toLink + '">Click here</a>'
	};

	transporter.sendMail(mailOptions, function (error, info) {
		if (error) {
			return console.log(error);
		}
		console.log('Message sent: ' + info.response);
	});
};
