var nconf = require('nconf');


var conf = nconf.file({file: __dirname + '/../../config/config-mail.json'});
var configMail = {
	configMail: conf.get('configMail')
};

module.exports.configMail = configMail;

var config = nconf.file({file: __dirname + '/../../config/config.json'});
var configHost = {
	host: config.get('host')
};
module.exports.host = configHost;
