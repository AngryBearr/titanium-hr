var nodemailer = require('nodemailer');

var mail = require('./mail-config.js').configMail;
var configHost = require('./mail-config.js').host;

module.exports.sendLinkToMail = function sendLinkToMail (token, email) {

	var transporter = nodemailer.createTransport(mail.configMail);
	var toLink = configHost.host + '#/recoveryPassword/' + token;
	var mailOptions = {
		from: 'nodemailertesttest@gmail.com',
		to: email, //
		subject: 'Recovery Password',
		text: toLink,
		html: '<b><p>Hello !</p><p>You have requested to change the password for your account Titanium-soft. In order to change the password ,</p></b> <a href="' + toLink + '"> click on the link.</a>'
	};

	transporter.sendMail(mailOptions, function (error, info) {
		if (error) {
			return console.log(error);
		}
		console.log('Message sent: ' + info.response);
	});
};
