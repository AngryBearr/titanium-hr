module.exports.genError = function (errorCode, msg) {
	return {status: errorCode, msg: msg};
};
