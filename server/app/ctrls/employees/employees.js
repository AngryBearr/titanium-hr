'use strict';

var mysql = require('mysql');
var jwt = require('jsonwebtoken');

var connectionParam = require('./../../modules/config.js').DBconf;
var resError = require('./../../modules/error-generator').genError;
var sendLinkToMail = require('./../../modules/send-new-user-link.js').sendLinkToMail;

var secret_key = 'superman';

function getAllUser (param, callback) {
	var connection = mysql.createConnection(connectionParam);

	connection.query('SELECT profile_picture,'+
			' employee_id,'+
			' first_name,'+
			' last_name,'+
			' departament,'+
			' grade,'+
			' role,'+
			' hire_date,'+
			' personal_phone,'+
			' company_phone,'+
			' personal_email,'+
			' company_email'+
			' FROM info_user',
			param.id, function (err, data) {

				if (err) {
					return callback(resError(400, 'Error from database.'));
				}

				return callback(null, JSON.stringify(data));
			});
}

module.exports.getAllUserApi = function getAllUserApi (req, res) {
	getAllUser(req.params, function getAllUser (err, data) {
		if (err) {
			res.status(err.status).send(err.msg);
		} else {
			res.header('Content-Type', 'application/json');
			res.send(data);
		}
	});
};

function addNewUser (param, callback) {
	var connection = mysql.createConnection(connectionParam);

	var token = jwt.sign(param, secret_key);

	if (!param.email) {
		return callback(resError(400, 'Please enter valid E-mail!'));
	}

	connection.query('INSERT INTO new_user SET email = ?, hash = ?', [param.email, token], function (err, data) {
		if (err) {
			return callback(resError(400, 'Error from database.'));
		}
		sendLinkToMail(token, param.email);
		return callback(null, JSON.stringify(data));
	});
}

module.exports.addNewUserApi = function addNewUserApi (req, res) {
	addNewUser(req.body, function addNewUser (err, data) {
		if (err) {
			res.status(err.status).send(err.msg);
		} else {
			// res.header('Content-Type', 'application/json');
			res.send('Link to add new user send to email!');
		}
	});
};
