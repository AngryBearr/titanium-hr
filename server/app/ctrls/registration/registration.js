'use strict';

var mysql = require('mysql');
var jwt = require('jsonwebtoken');
var md5 = require('md5');

var connectionParam = require('./../../modules/config.js').DBconf;
var resError = require('./../../modules/error-generator').genError;

var secret_key = 'superman';


function checkHash (param, callback) {

	var connection = mysql.createConnection(connectionParam);

	connection.connect();

	var user = jwt.verify(param.hash, secret_key);

	connection.query('SELECT * FROM new_user WHERE hash = ? AND email = ?', [param.hash, user.email], function (err, data) {


		if (err) {
			return callback(err);
		}

		if (data.length === 0) {
			return callback(resError(404, 'Your link has been expired'));
		}

		return callback(null);

	});

}

module.exports.checkHashApi = function checkHashApi (req, res) {
	checkHash(req.params, function resHandler (err) {
		if (err) {
			return res.status(err.status).send(err.msg);
		} else {
			res.send('Hash found in DB');
		}
	});
};

function deleteHash (hash) {
	var connection = mysql.createConnection(connectionParam);

	connection.connect();

	connection.query('UPDATE new_user SET hash = ? WHERE hash = ?', [null, hash]);
}

function registration (hash, body, checkId, callback) {
	var connection = mysql.createConnection(connectionParam);

	connection.connect();

	if (!body.password) {
		return callback(resError(409, 'Password is mandatory field'));
	}

	var user = jwt.verify(hash, secret_key);

	var pass = md5(body.password);

	connection.query('INSERT INTO hr_users SET password = ?, hash = ?, email = ?', [pass, null, user.email], function (err) {

		if (err) {
			return callback(err);
		}


		deleteHash(hash);

		callback(null, checkId(user.email, body, function (id, param) {
			var connection = mysql.createConnection(connectionParam);

			connection.connect();

			connection.query('INSERT INTO info_user SET id_user = ?, first_name = ?, last_name = ?',[id[0].id, param.firstName, param.lastName], function (err, data) {
				if (err) {
					return resError(400, 'Error from databases');
				}

				return data;
			});
		}));

	});
}

function checkId (email, param, cb) {
	var connection = mysql.createConnection(connectionParam);
	console.log(email);
	connection.query('SELECT id FROM hr_users WHERE email = ?', email, function (err, data) {
		if (err) {
			return err;
		}
		return cb(data, param);
	});
}



module.exports.registrationApi = function registrationApi (req, res) {
	var hash = req.params.hash;
	var body = req.body;

	registration(hash, body, checkId, function resHandler (err) {
		if (err) {
			return res.status(err.status).send(err.msg);
		} else {
			res.send('Registration success');
		}
	});
};
