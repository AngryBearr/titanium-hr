'use strict';

var mysql = require('mysql');
var fs = require('fs');

var connectionParam = require('./../../modules/config.js').DBconf;

module.exports.importSqlApi = function importSqlApi (req, res) {

	connectionParam.multipleStatements = true;

	var connection = mysql.createConnection(connectionParam);

	fs.readFile('server/db/titaniumhr.sql', 'utf8', function (err,data) {
		if (err) {
			return res.send(err);
		} else {
			connection.query(data, function (err) {
				if (err) {
					return res.send(err);
				}
			});
			return res.send('Database create succes');
		}
	});

};
