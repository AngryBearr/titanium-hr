'use strict';

var mysql = require('mysql');
var md5 = require('md5');
var jwt = require('jsonwebtoken');

var resError = require('./../../modules/error-generator').genError;
var connectionParam = require('./../../modules/config.js').DBconf;

var secret_key = 'superman';

function authorization (param, callback) {
	var connection = mysql.createConnection(connectionParam);

	connection.connect();

	if (!param.email || !param.password) {
		return callback(resError(409, 'E-mail and password are mandatory'));
	}

	var password = md5(param.password);

	connection.query('SELECT * FROM hr_users WHERE email = ? AND password = ?', [param.email, password], function (err, data) {

		if (err) {
			return callback(err);
		}

		if (!data.length) {
			return callback(resError(404, 'Your email or password is wrong.'));
		}

		var token = jwt.sign(data[0], secret_key);

		return callback(null, {token, data});
	});

	connection.end();

}

module.exports.authorizationApi = function authorizationApi (req, res) {
	authorization(req.body, function authHandler (err, data) {
		if (err) {
			return res.status(err.status).send(err.msg);
		} else {
			res.send(data);
		}
	});
};
