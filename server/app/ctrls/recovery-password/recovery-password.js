'use strict';

var mysql = require('mysql');
var jwt = require('jsonwebtoken');
var md5 = require('md5');

var connectionParam = require('./../../modules/config.js').DBconf;
var resError = require('./../../modules/error-generator').genError;

var secret_key = 'superman';


function checkHash (param, callback) {

	var connection = mysql.createConnection(connectionParam);

	connection.connect();

	var user = jwt.verify(param.hash, secret_key);

	connection.query('SELECT * FROM hr_users WHERE hash = ? AND email = ? AND id = ?', [param.hash, user.email, user.id], function (err, data) {


		if (err) {
			return callback(err);
		}

		if (data.length === 0) {
			return callback(resError(404, 'Your link has been expired'));
		}

		return callback(null);

	});

}

module.exports.checkHashApi = function checkHashApi (req, res) {
	checkHash(req.params, function resHandler (err) {
		if (err) {
			return res.status(err.status).send(err.msg);
		} else {
			res.send('Hash found in DB');
		}
	});
};

function recoveryPassword (hash, body, callback) {
	var connection = mysql.createConnection(connectionParam);

	connection.connect();

	if (!body.password) {
		return callback(resError(409, 'Password is mandatory field'));
	}

	var user = jwt.verify(hash, secret_key);

	var pass = md5(body.password);

	connection.query('UPDATE hr_users SET password = ?, hash = ? WHERE hash = ? AND email = ? AND id = ?', [pass, null, hash, user.email, user.id], function (err) {

		if (err) {
			return callback(err);
		}

		return callback(null);

	});
}

module.exports.recoveryPasswordApi = function recoveryPasswordApi (req, res) {
	var hash = req.params.hash;
	var body = req.body;

	recoveryPassword(hash, body, function resHandler (err) {
		if (err) {
			return res.status(err.status).send(err.msg);
		} else {
			res.send('Password changed successfully');
		}
	});
};
