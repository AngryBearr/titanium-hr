'use strict';

var mysql = require('mysql');
var jwt = require('jsonwebtoken');
var connectionParam = require('./../../modules/config.js').DBconf;
var resError = require('./../../modules/error-generator').genError;
var sendLinkToMail = require('./../../modules/send-recovery-link.js').sendLinkToMail;

// var nconf = require('nconf');


var secret_key = 'superman';

// function sendLinkToMail (token) {
//
// 	var transporter = nodemailer.createTransport(mail.configMail);
// 	var toLink = configHost.host + '#/recoveryPassword/' + token;
// 	var mailOptions = {
// 		from: 'nodemailertesttest@gmail.com',
// 		to: 'nichita.ursu.v@gmail.com', //
// 		subject: 'Recovery Password',
// 		text: toLink,
// 		html: '<b>Hell worldfjdsnfsdlnflsd</b> <a href="' + toLink + '">Click here</a>'
// 	};
//
// 	transporter.sendMail(mailOptions, function (error, info) {
// 		if (error) {
// 			return console.log(error);
// 		}
// 		console.log('Message sent: ' + info.response);
// 	});
//
// }

function forgotPassword (param, callback) {

	var connection = mysql.createConnection(connectionParam);

	connection.connect();

	if (!param.email) {
		return callback(resError(404, 'Please enter E-mail'));
	}

	connection.query('SELECT email, id FROM hr_users WHERE email = ?', param.email, function (err, data) {

		if (data.length === 0) {
			return callback(resError(404, 'User with such email was not found'));
		}

		var token = jwt.sign(data[0], secret_key);

		writeHash(token);

	});

	function writeHash (token) {
		connection.query('UPDATE hr_users SET hash = ? WHERE email = ?', [token, param.email], function (err, data) {

			if (err) {
				return callback(err);
			}

			if (data.changedRows === 0) {
				return callback(resError(404, 'User with such email was not found'));
			}
			sendLinkToMail(token, param.email);
			return callback(null, token);
		});
	}
}

module.exports.forgotPasswordApi = function forgotPasswordApi (req, res) {
	forgotPassword(req.body, function resHandler (err, token) {
		if (err) {
			return res.status(err.status).send(err.msg);
		} else {
			res.send(token);
		}
	});
};
