'use strict';

var mysql = require('mysql');
var _ = require('underscore');
var jwt = require('jsonwebtoken');

var connectionParam = require('./../../modules/config.js').DBconf;
var resError = require('./../../modules/error-generator').genError;

var secret_key = 'superman';

// function getBasicInfo (param, token, callback) {
// 	var connection = mysql.createConnection(connectionParam);
//
// 	var user = jwt.verify(token, secret_key);
//
// 	console.log(user);
//
// 	connection.query('SELECT * FROM info_user WHERE id_user = ?', user.id, function (err, data) {
// 		if (err) {
// 			return callback(err);
// 		}
//
// 		if (data.length === 0) {
// 			return callback(resError(404, 'User not found!'));
// 		}
//
// 		return callback(null, JSON.stringify(data));
// 	});
// }
//
// module.exports.getBasicInfoApi = function getBasicInfoApi (req, res) {
// 	getBasicInfo(req.params, req.headers.token, function getBasicInfo (err, data) {
// 		if (err) {
// 			return res.status(err.status).send(err.msg);
// 		} else {
// 			res.header('Content-Type', 'application/json');
// 			return res.send(data);
// 		}
// 	});
// };

function getBasicInfo (param, callback) {
	var connection = mysql.createConnection(connectionParam);

	connection.connect();

	connection.query('SELECT * FROM info_user', function (err, data) {
		if (err) {
			return callback(err);
		}

		if (data.length === 0) {
			return callback(resError(404, 'User not found!'));
		}

		return callback(null, JSON.stringify(data));
	});
}

module.exports.getBasicInfoApi = function getBasicInfoApi (req, res) {
	getBasicInfo(req.params, function getBasicInfo (err, data) {
		if (err) {
			return res.status(err.status).send(err.msg);
		} else {
			res.header('Content-Type', 'application/json');
			return res.send(data);
		}
	});
};



function updateBasicInfo (param, token, callback) {
	var connection = mysql.createConnection(connectionParam);

	var body = _.values(param);
	console.log(body);

	var user = jwt.verify(token, secret_key);

	if (user.id != param.id_user) {
		return callback(resError(400, 'You don\'t have rights to do that!'));
	}

	connection.query('UPDATE info_user set profile_picture = ?,'+
	' company_email = ?,'+
	' personal_email = ?,'+
	' company_phone = ?,'+
	' personal_phone = ?,'+
	' skype_id = ?,'+
	' departament = ?,'+
	' role = ?,'+
	'  grade = ?,'+
	' hire_date = ?,'+
	' current_project = ?,'+
	' employee_id = ?,'+
	' status = ?,'+
	' last_name = ?,'+
	' first_name = ?,'+
	' middle_name = ?,'+
	' nickname = ?,'+
	' birthday = ?,'+
	' idnp = ?,'+
	' gender = ?,'+
	' marital_status = ?,'+
	' country = ?,'+
	' city = ?,'+
	' street = ?,'+
	' languages = ?'+
	' WHERE id_user = ?',
	body, function (err, data) {
		console.log(err);
		if (err) {
			return callback(resError(400, 'Error from database.'));
		}

		if (data.changedRows === 0) {
			return callback(resError(401, 'User data is not updated'));
		}

		return callback(null, JSON.stringify(data));
	});
}

module.exports.updateBasicInfoApi = function updateBasicInfoApi (req, res) {
	updateBasicInfo(req.body, req.headers.token, function getBasicInfo (err) {
		if (err) {
			res.status(err.status).send(err.msg);
		} else {
			res.send('User data updated successfully');
		}
	});
};
