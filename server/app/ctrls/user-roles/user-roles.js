var userRoles = {
	SuperUser: 'SuperUser',
	HRManager: 'HR Manager',
	Employee: 'Employee'
};

module.exports.roles = function roles () {
	return userRoles;
};