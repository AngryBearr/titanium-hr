CREATE TABLE hr_users (
  id int AUTO_INCREMENT,
  email varchar(50),
  password varchar(100),
  hash text,
  PRIMARY KEY(id)
) DEFAULT CHARSET=utf8;

CREATE TABLE info_user (
  id int AUTO_INCREMENT,
  profile_picture varchar(500),
  company_email varchar(50),
  personal_email varchar(50),
  company_phone int,
  personal_phone int,
  skype_id varchar(50),
  departament varchar(100),
  role varchar(40),
  grade varchar(40),
  hire_date text,
  current_project varchar(50),
  employee_id int,
  status varchar(10),
  last_name varchar(30),
  first_name varchar(30),
  middle_name varchar(30),
  nickname varchar(20),
  birthday text,
  idnp bigint,
  gender varchar(10),
  marital_status varchar(50),
  country varchar(50),
  city varchar(50),
  street varchar(50),
  languages varchar(50),
  id_user int,
  FOREIGN KEY(id_user) REFERENCES hr_users(id),
  PRIMARY KEY(id)
) DEFAULT CHARSET=utf8;

CREATE TABLE new_user (
  id int AUTO_INCREMENT,
  email varchar(100),
  hash text,
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

INSERT INTO hr_users (email, password) VALUES ('admin@titanium-soft.com','5f4dcc3b5aa765d61d8327deb882cf99');
INSERT INTO info_user (profile_picture, company_email, personal_email, company_phone, personal_phone, skype_id, departament, role, grade, hire_date, current_project,
employee_id, status, last_name, first_name, middle_name, nickname, birthday, idnp, gender, marital_status, country, city, street, languages, id_user)
VALUES ('http://skarvit.ru/wp-content/uploads/2011/05/79_e6c2419c4d.jpg', 'lorem@ipsum', 'lorem@ipsum', '68028356', '22323232', 'loremipsum16', 'WebJS', 'Team Lead',
	'Middle', '05.05.2015', 'unknown', '1', 'active', 'Lipov', 'Alexandr', 'Unknown', 'lorem', '05.05.2015', '2004007894568', 'male', 'Single', 'Moldova', 'Chisinau',
	'Baker str. 221B', 'English,Russian', '1');

INSERT INTO hr_users (email, password) VALUES ('lipov@titanium-soft.com','5f4dcc3b5aa765d61d8327deb882cf99');
INSERT INTO info_user (profile_picture,
				 company_email,
				 personal_email,
				 company_phone,
				 personal_phone,
				 skype_id,
				 departament,
				 role,
				 grade,
				 hire_date,
				 current_project,
				 employee_id,
				 status,
				 last_name,
				 first_name,
				 middle_name,
				 nickname,
				 birthday,
				 idnp,
				 gender,
				 marital_status,
				 country,
				 city,
				 street,
				 languages,
				 id_user)
VALUES ('http://skarvit.ru/wp-content/uploads/2011/05/79_e6c2419c4d.jpg',
	   'lorem@ipsum',
	   'lorem@ipsum',
	   '68028356',
	   '22323232',
	   'loremipsum16',
	   'WebJS',
	   'Team Lead',
	   'Middle',
	   '05.05.2015',
	   'unknown',
	   '2',
	   'active',
	   'Lipov',
	   'Alexandr',
	   'Unknown',
	   'lorem',
	   '05.05.2015',
	   '2004007894568',
	   'male',
	   'Single',
	   'Moldova',
	   'Chisinau',
	   'Baker str. 221B',
	   'English,Russian',
	   '2');

INSERT INTO hr_users (email, password) VALUES ('antonov@titanium-soft.com','5f4dcc3b5aa765d61d8327deb882cf99');
INSERT INTO info_user (profile_picture, company_email, personal_email, company_phone, personal_phone, skype_id, departament, role, grade, hire_date, current_project,
employee_id, status, last_name, first_name, middle_name, nickname, birthday, idnp, gender, marital_status, country, city, street, languages, id_user)
VALUES ('http://skarvit.ru/wp-content/uploads/2011/05/79_e6c2419c4d.jpg', 'lorem@ipsum', 'lorem@ipsum', '68028356', '22323232', 'loremipsum16', 'WebJS', 'Team Lead',
	'Middle', '05.05.2015', 'unknown', '3', 'active', 'Antonov', 'Anton', 'Unknown', 'lorem', '05.05.2015', '2004007894568', 'male', 'Single', 'Moldova', 'Chisinau',
	'Baker str. 221B', 'English,Russian', '3');

INSERT INTO hr_users (email, password) VALUES ('bayanov@titanium-soft.com','5f4dcc3b5aa765d61d8327deb882cf99');
INSERT INTO info_user (profile_picture, company_email, personal_email, company_phone, personal_phone, skype_id, departament, role, grade, hire_date, current_project,
employee_id, status, last_name, first_name, middle_name, nickname, birthday, idnp, gender, marital_status, country, city, street, languages, id_user)
VALUES ('http://skarvit.ru/wp-content/uploads/2011/05/79_e6c2419c4d.jpg', 'lorem@ipsum', 'lorem@ipsum', '68028356', '22323232', 'loremipsum16', 'WebJS', 'Team Lead',
	'Middle', '05.05.2015', 'unknown', '4', 'active', 'Bayanov', 'Baklan', 'Unknown', 'lorem', '05.05.2015', '2004007894568', 'male', 'Single', 'Moldova', 'Chisinau',
	'Baker str. 221B', 'English,Russian', '4');

INSERT INTO hr_users (email, password) VALUES ('bobrov@titanium-soft.com','5f4dcc3b5aa765d61d8327deb882cf99');
INSERT INTO info_user (profile_picture, company_email, personal_email, company_phone, personal_phone, skype_id, departament, role, grade, hire_date, current_project,
employee_id, status, last_name, first_name, middle_name, nickname, birthday, idnp, gender, marital_status, country, city, street, languages, id_user)
VALUES ('http://skarvit.ru/wp-content/uploads/2011/05/79_e6c2419c4d.jpg', 'lorem@ipsum', 'lorem@ipsum', '68028356', '22323232', 'loremipsum16', 'WebJS', 'Team Lead',
	'Middle', '05.05.2015', 'unknown', '5', 'active', 'Bobrov', 'Babayan', 'Unknown', 'lorem', '05.05.2015', '2004007894568', 'male', 'Single', 'Moldova', 'Chisinau',
	'Baker str. 221B', 'English,Russian', '5');
