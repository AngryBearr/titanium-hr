var path = require('path');

function Routes (app) {
	'use strict';

	app.get('/', function (req, res) {
		res.sendFile(path.resolve(__dirname + './../public/index.html'));
	});

	app.get('/test', function (req, res) {
		res.send('Test route');
	});

	var login = require('./app/ctrls/login/login');
	app.post('/login', login.authorizationApi);

	var importSql = require('./app/ctrls/import-sql/import-sql.js');
	app.get('/importSql', importSql.importSqlApi);

	var mainPage = require('./app/ctrls/main-page/main-page.js');
	app.get('/basic-info', mainPage.getBasicInfoApi);
	// app.get('/basic-info', mainPage.getBasicInfoApi);
	app.put('/basic-info', mainPage.updateBasicInfoApi);

	var employees = require('./app/ctrls/employees/employees.js');
	app.get('/employees', employees.getAllUserApi);
	app.post('/employees', employees.addNewUserApi);

	var forgotPassword = require('./app/ctrls/forgot-password/forgot-password.js');
	app.post('/forgotPassword', forgotPassword.forgotPasswordApi);

	var recoveryPassword = require('./app/ctrls/recovery-password/recovery-password.js');
	app.get('/recoveryPassword/:hash', recoveryPassword.checkHashApi);
	app.put('/recoveryPassword/:hash', recoveryPassword.recoveryPasswordApi);

	var registration = require('./app/ctrls/registration/registration.js');
	app.get('/registration/:hash', registration.checkHashApi);
	app.post('/registration/:hash', registration.registrationApi);

}


module.exports.routes = Routes;
