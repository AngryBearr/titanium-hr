var gulp = require('gulp');
var clean = require('gulp-clean');
var inject = require('gulp-inject');
var concat = require('gulp-concat');
var sass = require('gulp-sass');

gulp.task('sass', function () {
	return gulp.src('./source/scss/**/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('./public/css'));
});

gulp.task('cssLibs', function () {
	var cssLibs = [
		'./bower_components/normalize-css/normalize.css',
		'./bower_components/components-font-awesome/css/font-awesome.min.css',
		'./bower_components/ngToast/dist/ngToast-animations.min.css',
		'./bower_components/angular-loading-bar/src/loading-bar.css'
	];
	gulp.src(cssLibs)
		.pipe(gulp.dest('./public/libs'));
});

gulp.task('copyFonts', function () {
	var fonts = [
		'./source/fonts/*',
		'./bower_components/components-font-awesome/fonts/*.ttf',
		'./bower_components/components-font-awesome/fonts/*.otf'
	];
	gulp.src(fonts)
		.pipe(gulp.dest('./public/fonts'));
});

gulp.task('tpl', function () {
	return gulp.src('./source/app/templates/**/*.html')
		.pipe(gulp.dest('./public/templates'));
});

gulp.task('clean', function () {
	return gulp.src([
		'./public/libs',
		'./public/app'
	], {read: false})
		.pipe(clean());
});

gulp.task('copyLibs', function () {

	var libs = [
		'./bower_components/underscore/underscore-min.js',
		'./bower_components/angular/angular.min.js',
		'./bower_components/angular-ui-router/release/angular-ui-router.min.js',
		'./bower_components/angular-sanitize/angular-sanitize.js',
		'./bower_components/angular-translate/angular-translate.js',
		'./bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
		'./bower_components/angular-validation-ghiscoding/dist/angular-validation.min.js',
		'./bower_components/ngToast/dist/ngToast.js',
		'./bower_components/angular-animate/angular-animate.js',
		'./bower_components/angular-loading-bar/src/loading-bar.js',
		'./bower_components/moment/min/moment.min.js'

	];

	var addData = [
		'./bower_components/angular-validation-ghiscoding/locales/validation/**/*.json'
	];

	gulp.src(libs)
		.pipe(gulp.dest('./public/libs'));

	gulp.src(addData)
		.pipe(gulp.dest('./public/assets/validation'));
});

gulp.task('copyApp', function () {
	var appDir = './source/app/';

	var appFiles = [
		appDir + 'app.js',
		appDir + 'routes.js',
		appDir + '**/*.js'
	];

	return gulp.src(appFiles)
		.pipe(concat('hr.js'))
		.pipe(gulp.dest('./public/app'));
});

gulp.task('copyAppDev', function () {
	var appDir = './source/app/';

	var appFiles = [
		appDir + 'app.js',
		appDir + 'routes.js',
		appDir + '**/*.js'
	];

	return gulp.src(appFiles)
		.pipe(gulp.dest('./public/app'));
});

gulp.task('inject', function () {

	var libsDir = './public/libs/';

	var target = gulp.src('./public/index.html');

	var jsLibs = gulp.src([
		libsDir + 'underscore-min.js',
		libsDir + 'angular.min.js',
		libsDir + 'angular-sanitize.js',
		libsDir + 'ngToast.js',
		libsDir + 'angular-translate.js',
		libsDir + 'angular-translate-loader-static-files.js',
		libsDir + 'angular-validation.min.js',
		libsDir + 'angular-ui-router.min.js',
		libsDir + 'angular-animate.js',
		libsDir + 'loading-bar.js',
		libsDir + 'moment.min.js'
	]);

	var app = gulp.src('./public/app/**/*.js');

	var cssLibs = gulp.src(libsDir + '*.css');

	var css = gulp.src('./public/css/style.css');

	return target.pipe(inject(jsLibs, {
		relative: false,
		starttag: '<!-- inject:libs:js -->'
	})).pipe(inject(cssLibs, {
		relative: false,
		starttag: '<!-- inject:libs:css -->'
	})).pipe(inject(css, {
		relative: false,
		starttag: '<!-- inject:app:css -->'
	})).pipe(inject(app, {
		relative: false,
		starttag: '<!-- inject:app:{{ext}} -->'
	})).pipe(gulp.dest('./public'));

});


gulp.task('build', ['clean', 'sass', 'tpl'], function () {
	gulp.start(['copyLibs', 'copyApp', 'cssLibs']);
});

gulp.task('build-dev', ['clean', 'sass', 'tpl'], function () {
	gulp.start(['copyLibs', 'copyAppDev', 'cssLibs']);
});

gulp.task('watch', function () {
	gulp.watch(['./source/**'], ['build-dev']);
});

gulp.task('watch-sass', function () {
	gulp.watch(['./source/scss/**'], ['sass']);
});
