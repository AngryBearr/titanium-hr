Russian RD - https://docs.google.com/document/d/16LOkXb3sqcpj2_LHHyGXTe0EwTEGHcbH7YCP-wkNHkM/edit?usp=sharing

#Instrunctions for Windows* :
	...

#Instrunctions for MacOS* :
	...
#Instrunctions for Linux*:

	1.) Programs must be installed for using our project :

		a.) NodeJS (In terminal) :
			Step-1 : 'sudo apt-get install curl' ;
			Step-2 : 'curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -' ;
			Step-3 : 'sudo apt-get install -y nodejs' ;

		b.) MySql (In terminal) :
			Step-1 : 'sudo apt-get install mysql-client' ;
			Step-2 : 'sudo apt-get install mysql-server' ;

		c.) Git (In terminal) :
			Step-1 : 'sudo apt-get install git' ;

		d.) Bower (In terminal) :
			Step-1 : 'npm install -g bower' ;

		e.) Gulp (In terminal) :
			Step-1 : 'npm install -g gulp' ;

		f.) Nodemon (In terminal) :
			Step-1 : 'npm install -g nodemon' ;

	2.) Clone this Project (In terminal) :

		a.) 'git clone https://YOUR_USER_NAME@bitbucket.org/AngryBearr/titanium-hr.git' (YOUR_USER_NAME --> Means that you have to have an bitbucket account registred. When you clone a repository it will ask your password!) ;
		b.) 'cd titanium-hr' ;
	3.) Server (In terminal) :

		a.) 'bower install' ;
		b.) 'npm install' ;
		c.) 'gulp build-dev' ;
		d.) 'gulp inject' ;
		e.) 'nodemon server/index.js' ;

	4.) Database (In terminal, browser, file system) :

		a.) Open new terminal window and type: 'mysql -u (your user name, for example - 'root' ) -p' ;
		b.) 'CREATE DATABASE titanium_hr ;' ;
		c.) Open browser and go to address: http://127.0.0.1:8001/ImportSql ;
		d.) In your home folder, find a folder 'titanium_hr' ;
		e.) Inside 'titanium_hr' folder go to 'server' then go to 'config' folder, then open db-config.json in your favorit text editor.
		f.) Edit that file, so it reflects your Database configurations: 'user: Your_Username_In_MySQL', 'password: Password_From_Your_Username' ;

	5.) Start project (In terminal, browser) :

		a.) Go to terminal where you you've started 'nodemon' ;
		d.) Open browser and go to address: http://127.0.0.1:8001 ;

#Authentication credentials :

	Login : 'admin@titanium-soft.com' ;
	Password : 'password' ;